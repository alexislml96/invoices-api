using Xunit;
using DeLaSalle.Invoices.Entities;

namespace DeLaSalle.Invoices.Tests.Entities;

public class CustomerTest
{
    [Fact]
    public void Name_WhenSetNameValue_ReturnsSameValue()
    {
        //Arrange
        var expected = "DeLaSalle Campestre";
        var sut = new Customer();

        //Act
        sut.Name = expected;
        
        var result = sut.Name;
        
        //Assert
        Assert.Equal(expected,result);
        
    }

    [Fact]
    public void Rfc_WhenSetRfcValue_ReturnSameValue()
    {
        //Arrange
        var expectedValue = "Abcd12345";
        var sut = new Customer();

        //Act
        sut.Rfc = expectedValue;
        var result = sut.Rfc;

        //Assert
        Assert.Equal(expectedValue,result);
    }

    [Fact]
    public void Phone_WhenSetPhoneValue_ReturnSameValue()
    {
        //Arrange
        var expectedValue = "4741095920";
        var sut = new Customer();

        //Act
        sut.Phone = expectedValue;
        var result = sut.Phone;

        //Assert
        Assert.Equal(expectedValue,result);
    }

    [Fact]
    public void Email_WhenSetEmailValue_ReturnSameValue()
    {
        //Arrange
        var expectedValue = "alexislml96@gmail.com";
        var sut = new Customer();
    
        //Act
        sut.Email = expectedValue;
        var result = sut.Email;

        //Assert
        Assert.Equal(expectedValue,result);
    }
}