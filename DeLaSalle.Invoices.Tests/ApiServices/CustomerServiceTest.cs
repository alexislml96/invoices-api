using Xunit;
using DeLaSalle.Invoices.Api.Services;
using DeLaSalle.Invoices.Entities;
using System.Collections.Generic;
using Moq;
using DeLaSalle.Invoices.Api.Repositories.Interfaces;
using System.Threading.Tasks;

namespace DeLaSalle.Invoices.Tests.ApiServices;
public class CustomerServiceTest
{
    [Fact]
    public async Task GetAllAsync_WhenGetList_ReturnList()
    {
        //arrange
        var expected = new List<Customer>
        {
            new Customer{Id=1,Name="Customer 1"},
            new Customer{Id=1,Name="Customer 2"}
        };

        var mock = new Mock<ICustomerRepository>();
        mock.Setup(m=> m.GetAllAsync()).Returns(Task.FromResult(expected));
        
        var sut = new CustomerService(mock.Object);
        
        //act

        var res = await sut.GetAllAsync();

        //assert

        Assert.Equal(expected,res);
    }

     [Fact]
     public async Task UpdateAsync_WhenUpdateCustomer_ReturnsSameCustomer()
     {
         //arrange
            var customer = new Customer{Id = 1};
            var mock = new Mock<ICustomerRepository>();

            mock.Setup(m => m.UpdateAsync(customer)).Returns(Task.FromResult(customer));
            var sut = new CustomerService(mock.Object);
         //act
            var result = await sut.UpdateAsync(customer);

         //assert
            Assert.Equal(customer,result);
     }

     [Fact]
     public async Task SaveAsync_WhenSaveCustomer_ReturnCustomerWithIdGreaterThanZero()
     {
         //arrange
            var customer = new Customer{Id = 0};
            var expected = new Customer{Id = 1};
            var mock = new Mock<ICustomerRepository>();

            mock.Setup(m=> m.SaveAsync(customer)).Returns(Task.FromResult(expected));
            var sut = new CustomerService(mock.Object);

         //act
            customer = await sut.SaveAsync(customer);

         //assert
            Assert.Equal(expected.Id, customer.Id);

     }

     [Fact]
     public async Task DeleteAsync_WhenDeleteFails_ReturnFalse()
     {
         //arrange
            var expected = false;
            var customer = new Customer{Id = 1};
            var mock = new Mock<ICustomerRepository>();

            mock.Setup(m=> m.DeleteAsync(customer.Id)).Returns(Task.FromResult(expected));
            var sut = new CustomerService(mock.Object);
         //act
            var result = await sut.DeleteAsync(customer.Id);
         //assert
            Assert.Equal(expected,result);
     }

     [Fact]
     public async Task DeleteAsync_WhenDelete_ReturnsTrue()
     {
         //arrange
            var expected = true;
            var customer = new Customer{Id = 1};
            var mock = new Mock<ICustomerRepository>();

            mock.Setup(m=> m.DeleteAsync(customer.Id)).Returns(Task.FromResult(expected));
            var sut = new CustomerService(mock.Object);
         //act
            var result = await sut.DeleteAsync(customer.Id);
         //assert
            Assert.Equal(expected,result);
     }

     [Fact]
     public async void GetCustomerByIdAsync_WhenGetCustomer_ReturnsCustomer()
     {
         //arrange
            var id = 1;
            var expected = new Customer{Id = id};
            var mock = new Mock<ICustomerRepository>();

            mock.Setup(m=> m.GetCustomerByIdAsync(id)).Returns(Task.FromResult(expected));
            var sut = new CustomerService(mock.Object);
         //act
            var result = await sut.GetCustomerByIdAsync(id);
         //assert
            Assert.Equal(expected,result);
     }
}
