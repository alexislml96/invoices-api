import axios, {AxiosResponse} from 'axios';

import ICustomer from '../entities/customer';
import ICustomerFilter from '../entities/Http/customerFilter';

axios.defaults.baseURL = "https://localhost:7264/api/";

const responseBody = (response:AxiosResponse) => response.data;

const request = {
    get:(url:string, filters:{} = {}) => {
        const params = new URLSearchParams(filters);
        return axios.get(url,{params}).then(responseBody)
    },
    post:(url:string,body:{}) => axios.post(url,body).then(responseBody),
    put:(url:string,body:{}) => axios.put(url,body).then(responseBody),
    delete:(url:string) => axios.delete(url).then(responseBody)
}


const Customer = {
    list: () => request.get('customer'),
    search : (filters:ICustomerFilter) => request.get('customer/search', filters),
    create: (customer:ICustomer) => request.post('customer',customer),
    update: (customer:ICustomer) => request.put('customer',customer),
    delete: (id:number) => request.delete(`customer/${id}`),
}

const Country ={
    listSateByCountry : (countryCode:string) => request.get(`country/${countryCode}/states`)
}

const api = {
    axios,
    Customer,
    Country
}

export default api;