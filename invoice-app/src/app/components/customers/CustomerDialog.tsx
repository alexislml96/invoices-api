import { observer } from 'mobx-react-lite';
import React, { useState } from 'react';
import { useStore } from '../../store/store';

const CustomerDialog = () => {
    const { customerStore } = useStore();
    const { customerId, deleteCustomerAsync, cancelDeleteEvent } = customerStore;

    return (
        <React.Fragment>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Delete customer {customerId}</h5>
                        </div>
                        <div className="modal-body">
                            <p>Are you sure to delete this customer?</p>
                        </div>
                        <div className="modal-footer">
                        <button className='btn btn-primary' onClick={() => deleteCustomerAsync(customerId)}>Accept</button>
                        <button className='btn btn-danger' onClick={() => cancelDeleteEvent()}>Cancel</button>
                        </div>
                    </div>
        </React.Fragment>
    );
}

export default observer(CustomerDialog);