import { observer } from 'mobx-react-lite';
import React, { useState } from 'react';
import ICustomer from '../../entities/customer';
import ICustomerFilter from '../../entities/Http/customerFilter';
import IState from '../../entities/state';
import { useStore } from '../../store/store';

const CustomerList = () => {

    const {customerStore} = useStore();
    const {customers, editCustomerEvent, deleteCustomerEvent, loadCustomers, countryStates} = customerStore;
    const [filter,setFilters] = useState<ICustomerFilter>({name:'', stateCode:''});

    const hanldeInputChanges = (event:any) =>{
        const{name,value} = event.target;

        setFilters({...filter,[name]:value})
    }

    const handleCountryStateChange = (event:any)=>{
        const{name,value} = event.target;

        setFilters({...filter,stateCode:value})


    }
    
    return (
        <React.Fragment>

            <h3 className="mt-3 mb-3">Customers</h3>

            <div className="row mb-3">
                <div className='input-group'>
                    <input type="text" name='name' id='name' value={filter.name} onChange={hanldeInputChanges} className="form-control" placeholder="Name or RFC" aria-label="First name" />
                    <select id='stateCode' onChange={handleCountryStateChange} name='stateCode' className='form-control'>
                    <option key='' value=''>Todos</option>
                    {
                        countryStates.map((state:IState) => (
                            <option key={state.code} value={state.code}>{state.name}</option>
                        ))
                    }
                    </select>
                    <button className='btn btn-primary' onClick={()=> loadCustomers(filter)}>Search</button>
                </div>

            </div>
            <div className="row mb-3">
                <div className="col-md-12 text-right">
                    <button type="button" className="btn btn-success" onClick={()=> editCustomerEvent(null)}>New</button>
                </div>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">RFC</th>
                        <th scope="col">State</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
        {
            customers.map((customer:ICustomer) =>(
                <tr key={customer.id}>
                    <th scope="row">{customer.id}</th>
                    <td>{customer.name}</td>
                    <td>{customer.rfc}</td>
                    <td>{customer.state.name}</td>
                    <td>
                        <button type='button' className='btn btn-primary' onClick={() => editCustomerEvent(customer)}>Edit</button>
                        <button type='button' className='btn btn-danger mx-1' onClick={() => deleteCustomerEvent(customer)} >Delete</button>
                    </td>
                </tr>
            ))

        }
        </tbody>

            </table>
        </React.Fragment>
    );
}

export default observer(CustomerList);