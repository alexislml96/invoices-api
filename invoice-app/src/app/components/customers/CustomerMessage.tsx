import { observer } from 'mobx-react-lite';
import React, { useState } from 'react';
import { useStore } from '../../store/store';

const CustomerMessage = () => {
    const { customerStore } = useStore();
    const {cancelErrorMessage} = customerStore;
    return (
        <React.Fragment>
            <div className="alert alert-danger alert-dismissible fade show">
                <strong>An error has occurred!</strong> Please try again later.
                <button type="button" onClick={()=> cancelErrorMessage()} className="btn-close" aria-label="Close"></button>
            </div>
        </React.Fragment>
    );
}

export default observer(CustomerMessage);