import React from 'react';

import CustomerList from './CustomerList';
import CustomerEdit from './CustomerEdit';
import CustomerDialog from './CustomerDialog';
import ICustomer from '../../entities/customer';

import { useStore } from '../../store/store';
import { observer } from 'mobx-react-lite';
import CustomerMessage from './CustomerMessage';

const CustomersDashboard = () => {

    const {customerStore} = useStore();
    const {editCustomer, isListLoaded, deleteCustomer, showMessage} = customerStore;
    const {errorMessage} = customerStore;

    if(errorMessage.length){
        return <div>An error has ocurred {errorMessage}</div>
    }

    return (
        <React.Fragment>
            {
            editCustomer === false && isListLoaded === true && deleteCustomer === false && 
            showMessage === false && <CustomerList/>
            }
            {
            editCustomer && isListLoaded === true && deleteCustomer === false && <CustomerEdit/>
            }
            {
                isListLoaded === false && <div>Loading...</div>
            }
            {
                deleteCustomer && isListLoaded === true && showMessage === false && <CustomerDialog/>
            }
            {
                showMessage && isListLoaded === true && deleteCustomer === false && <CustomerMessage/>
            }
        </React.Fragment>
    );
}

export default observer(CustomersDashboard);