import { observer } from 'mobx-react-lite';
import React, { useState } from 'react';
import ICustomer from '../../entities/customer';
import IState from '../../entities/state';
import { useStore } from '../../store/store';

const CustomerEdit = () => {

    const {customerStore} = useStore();
    const {selectedCustomer, saveCustomerEvent, cancelEditEvent, countryStates} = customerStore;
    
    let defaultState = {
        code: '',
        countryCode:'',
        name:'',
        taxAuthorityCode:''
    }

    let defaultCustomer={
        id:0,
        name:'',
        rfc:'',
        phone:'',
        email:'',
        address:'',
        stateCode: '',
        state : defaultState
    };

    let customerRef:ICustomer = (selectedCustomer !=null) ? selectedCustomer : defaultCustomer;

    let label = customerRef.id === 0 ? 'New Customer' : 'Edit Customer';

    const[customer,setCustomer] = useState<ICustomer>(customerRef);

    const handleInputChange = (event:any)=>{
        const{name,value} = event.target;
        
        setCustomer({...customer,[name]:value});
    }

    const handleCountryStateChange = (event:any)=>{
        const {value} = event.target;
        setCustomer({...customer, 'stateCode':value})
    }
    
    return (
        <React.Fragment>
            <h3 className="mt-3 mb-3">{label}</h3>
            <form>
                <div className="mb-3">
                    <label  className="form-label">Name</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.name} id="name" name='name' />
                </div>
                <div className="mb-3">
                    <label className="form-label">RFC</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.rfc} id="rfc" name='rfc' />
                </div>
                <div className="mb-3">
                    <label className="form-label">Phone</label>
                    <input onChange={handleInputChange} type="text" className="form-control" id="phone" value={customer.phone} name='phone'/>
                </div>

                <div className="mb-3">
                    <label className="form-label">Address</label>
                    <input onChange={handleInputChange} type="text" className="form-control" id="address" value={customer.address} name='address'/>
                </div>

                <div className="mb-3">
                    <label className="form-label">Email</label>
                    <input onChange={handleInputChange} type="email" className="form-control" id="email" value={customer.email} name='email'/>
                </div>

                <div className="mb-3">
                    <label className="form-label">State</label>
                    <select id='state' onChange={handleCountryStateChange} value={customer.stateCode} className='form-control'>
                    {
                        countryStates.map((state:IState) => (
                            <option key={state.code} value={state.code}>{state.name}</option>
                        ))
                    }
                    </select>
                    
                </div>
            
                <button type="button" onClick={() => saveCustomerEvent(customer)} className="btn btn-primary">Submit</button>
                <button type='button' onClick={() => cancelEditEvent()} className='btn btn-dark m-2'>Cancel</button>
            </form>
        </React.Fragment>
    );
}

export default observer(CustomerEdit);