import React, { useEffect, useState } from 'react';
import api from '../api/api';

import CustomersDashboard from '../components/customers/CustomersDashboard';
import ICustomer from '../entities/customer';

import { useStore } from '../store/store';
import { observer } from 'mobx-react-lite';

const Customers = () => {

    // const[editCustomer, setEditCustomer] = useState<boolean>(false);
    // const[isListLoaded, setIsListLoaded] = useState<boolean>(false);
    // const[customers, setCustomers] = useState<ICustomer[]>([]);
    // const[selectedCustomer, setSelectedCustomer] = useState<ICustomer|null>(null);

    const {customerStore} = useStore();

    useEffect(()=>{
        customerStore.loadCustomers({name:'', stateCode:''});
    }, []);

    return (
        <React.Fragment>
        <CustomersDashboard/>
        </React.Fragment>
    );
}

export default observer(Customers);