import { createContext, useContext } from "react";
import CustomerStore from "./customerStore";

interface IStore{
    customerStore: CustomerStore
}

export const store : IStore = {
    customerStore : new CustomerStore()
};

export const StoreContext = createContext(store);

export function useStore(){
    return useContext(StoreContext);
}