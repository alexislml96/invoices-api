import { action, makeAutoObservable, makeObservable, observable } from 'mobx';

import api from '../api/api';
import ICustomer from '../entities/customer';
import ICustomerFilter from '../entities/Http/customerFilter';
import IState from '../entities/state';

export default class CustomerStore {
    editCustomer: boolean = false;
    isListLoaded: boolean = false;
    customers: ICustomer[] = [];
    countryStates : IState[] = [];
    selectedCustomer: ICustomer | null = null;
    deleteCustomer : boolean = false;
    customerId : number = 0;
    showMessage:boolean = false;
    errorMessage:String = '';

    constructor() {
        makeAutoObservable(this);
    }

    addError(error:string){
        this.errorMessage = error;
    }

    loadCustomers = async (filters:ICustomerFilter) => {
        try {
            const responseCustomer = await api.Customer.search(filters);
            const responseCountryState = await api.Country.listSateByCountry('MX');
            
            this.isListLoaded = true;
            this.customers = responseCustomer.data;
            this.countryStates = responseCountryState.data;

            console.log(filters);

        }
        catch (error) {
            this.addError(String(error))
        }
    }

    editCustomerEvent = (customer: ICustomer | null) => {
        this.editCustomer = true;
        this.selectedCustomer = customer;
    }

    cancelErrorMessage = () => {
        this.showMessage = false;
    }

    cancelEditEvent = () => {
        this.editCustomer = false;
        this.selectedCustomer = null;
    }

    deleteCustomerEvent = (customer: ICustomer) =>{
        this.deleteCustomer = true;
        this.customerId = customer.id;
    }

    cancelDeleteEvent = () =>{
        this.deleteCustomer = false;
        this.customerId = 0;
    }

    saveCustomerEvent = async (customer: ICustomer) => {
        try {
            if (customer?.id === 0) {
                const response = await api.Customer.create(customer);
                
                this.customers.push(response.data)
            }
            else {
                const response = await api.Customer.update(customer);
                let index = this.customers.findIndex(u => u.id === customer.id);
                this.customers[index] = customer;
            }

            this.editCustomer = false;
            this.selectedCustomer = null;
        }
        catch(error)
        {
            this.addError(String(error))
        }
    }

    deleteCustomerAsync = async (id:number) =>{
        try
        {
            const response = await api.Customer.delete(id);

            if(response.data)
            {
                let index = this.customers.findIndex(u=> u.id === id);
                this.customers.splice(index, 1);
                this.deleteCustomer = false;
                this.customerId = 0;
            }
            else
            {
                this.deleteCustomer = false;
                this.customerId = 0;
                this.showMessage = true;
            }
        }
        catch(error)
        {
            this.addError(String(error))
        }
    }
}