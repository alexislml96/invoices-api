using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeLaSalle.Invoices.Entities.Http;

    public class CustomerFilter : BaseFilter
    {
        public string? Name{get;set;}
        public string? Code{get;set;}
        
        protected override void SetParameters()
        {
            base.SetParameters();
            _parameters["Name"] = Name;
            _parameters["Code"] = Code;
        }
    }
