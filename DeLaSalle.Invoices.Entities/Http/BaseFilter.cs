using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeLaSalle.Invoices.Entities.Http;
    public class BaseFilter
    {

        public BaseFilter()
        {
            _parameters = new Dictionary<string, string>();
        }

        public BaseFilter(int pageNumber, int pageSize)
        {
            
        }

        protected Dictionary<string,string> _parameters;
        public Dictionary<string,string> Parameters
        {
            get 
            {
                SetParameters();
                return _parameters; 
            }
        }

        protected virtual void SetParameters()
        {

        }
        
    }
