using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeLaSalle.Invoices.Entities;
public class AuthEnt
{
    public string UserName { get; set; }
    public string Password { get; set; }
}