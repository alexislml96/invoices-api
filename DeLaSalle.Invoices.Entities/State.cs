//using Dapper.Contrib.Extensions;
namespace DeLaSalle.Invoices.Entities;

public class State
{
    //[ExplicitKey]
    public string Code {get;set;}
    public string CountryCode{get;set;}
    public string Name{get;set;}
    public string TaxAuthorityCode{get;set;}
}