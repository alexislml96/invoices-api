using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeLaSalle.Invoices.Entities;
public class UserSession
{
    public int UserId { get; set; }
    public string Token { get; set; }
}