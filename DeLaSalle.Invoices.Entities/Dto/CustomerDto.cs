using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeLaSalle.Invoices.Entities.Dto;
    public class CustomerDto : Customer
    {
        public State State {get;set;}
    }