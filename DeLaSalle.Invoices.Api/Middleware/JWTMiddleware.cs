using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using DeLaSalle.Invoices.Api.Services.Interfaces;

namespace DeLaSalle.Invoices.Api.Middleware;
    public class JWTMiddleware
    {
        private readonly RequestDelegate _next;
        public JWTMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IAuthService authService)
        {
            try
            {
                var token = context.Request.Headers["Authorization"].FirstOrDefault().Split(" ").Last();
                if(token != null)
                    AttachUserToContext(context,authService, token);
            }
            catch
            {

            }
            await _next(context);
        }

        private void AttachUserToContext(HttpContext context, IAuthService authService, string token)
    {
        try
        {
            var secret = "THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACTE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING";
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = System.Text.Encoding.ASCII.GetBytes(secret);
            tokenHandler.ValidateToken(token, new Microsoft.IdentityModel.Tokens.TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero,
            }, out SecurityToken validatedToken);
            var jwtToken = (JwtSecurityToken)validatedToken;
            var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);
            context.Items["user"] = authService.GetUserById(userId).Result;
        }
        catch (Exception e)
        {
            System.Console.WriteLine(e.Message);
        }
    }
    }