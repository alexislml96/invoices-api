using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeLaSalle.Invoices.Api.Services.Interfaces;
using DeLaSalle.Invoices.Entities;
using Microsoft.AspNetCore.Mvc;

namespace DeLaSalle.Invoices.Api.Controllers;
[ApiController]
[Route("api/[controller]")]
public class AuthController : ControllerBase
{
    private readonly IAuthService _authService;
    public AuthController(IAuthService authService)
    {
        _authService = authService;
    }
    [HttpPost]
    public async Task<ActionResult<UserSession>> Auth([FromBody] AuthEnt auth)
    {
        var session = new UserSession();

        if (auth == null)
            return BadRequest();

        session = await _authService.GetAuthorization(auth);
        return session;
    }
}