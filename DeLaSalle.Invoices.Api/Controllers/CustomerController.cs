using Microsoft.AspNetCore.Mvc;
using DeLaSalle.Invoices.Entities;
using DeLaSalle.Invoices.Api.Responses;
using DeLaSalle.Invoices.Api.Services.Interfaces;
using DeLaSalle.Invoices.Entities.Dto;
using DeLaSalle.Invoices.Entities.Http;

namespace DeLaSalle.Invoices.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public async Task<ActionResult<Response<IEnumerable<Customer>>>> GetAll()
        {
            var response = new Response<IEnumerable<Customer>>();
            var customers = await _customerService.GetAllAsync();
            response.Data = customers;

            return Ok(response);
        }

        [HttpGet("search")]
        public async Task<ActionResult<Response<IEnumerable<CustomerDto>>>> Search([FromQuery] CustomerFilter filter)
        {
            var response = new Response<IEnumerable<CustomerDto>>();
            var customers = await _customerService.SearchAsync(filter);
            response.Data = customers;

            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult<Response<Customer>>> Save([FromBody] Customer customer)
        {
            var response = new Response<Customer>();
            customer = await _customerService.SaveAsync(customer);
            response.Data = customer;

            return Created($"api/customer/{customer.Id}", response);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Response<Customer>>> GetById(int id)
        {
            
            var response = new Response<Customer>();
            var customers = await _customerService.GetCustomerByIdAsync(id);
            response.Data = customers;

            if(customers == null)
            {
                response.Message = "Customer not found";
                response.Errors.Add("The customer id provided was not found");

                return NotFound(response);
            }

            return Ok(response);

        }

        [HttpPut]
        public async Task<ActionResult<Response<Customer>>> Update([FromBody] Customer customer)
        {
            var response = new Response<Customer>();
            customer = await _customerService.UpdateAsync(customer);
            response.Data = customer;

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Response<bool>>> Delete(int id)
        {
            
            var response = new Response<bool>();
            response.Data = await _customerService.DeleteAsync(id);
  
            return Ok(response);

        }

    }
}