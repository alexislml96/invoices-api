

using Dapper;
using Dapper.Contrib.Extensions;
using DeLaSalle.Invoices.Api.DataAccess.Interfaces;
using DeLaSalle.Invoices.Api.Repositories.Interfaces;
using DeLaSalle.Invoices.Entities;

namespace DeLaSalle.Invoices.Api.Repositories;


public class StateRepository : IStateRepository
{
    private readonly IData _data;


    public StateRepository(IData data)
    {
        _data = data;
    }

    public async Task<List<State>> GetByCountryAsync(string countryCode)
    {
        var sql = "SELECT * FROM State WHERE CountryCode = @CountryCode";

        var states = 
        (await _data.DbConnection.QueryAsync<State>(sql, new { CountryCode = countryCode})).ToList();

        return states;
    }

}