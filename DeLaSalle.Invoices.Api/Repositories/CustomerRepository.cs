using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeLaSalle.Invoices.Api.Repositories.Interfaces;
using DeLaSalle.Invoices.Entities;
using DeLaSalle.Invoices.Api.DataAccess.Interfaces;
using Dapper;
using Dapper.Contrib;
using Dapper.Contrib.Extensions;
using DeLaSalle.Invoices.Entities.Dto;
using System.Dynamic;
using DeLaSalle.Invoices.Entities.Http;

namespace DeLaSalle.Invoices.Api.Repositories;

public class CustomerRepository : ICustomerRepository
{
    private readonly IData _data;
    public CustomerRepository(IData data)
    {
        _data = data;
    }
    public async Task<bool> DeleteAsync(int id)
    {
        return await _data.DbConnection.DeleteAsync<Customer>(new Customer{Id = id});
    }

    public async Task<List<Customer>> GetAllAsync()
    {
        var sql = "SELECT * FROM Customer";
        var customers = (await _data.DbConnection.QueryAsync<Customer>(sql)).ToList();

        return customers;
    }

    public async Task<Customer> GetCustomerByIdAsync(int id)
    {
        var sql = "SELECT * FROM Customer WHERE Id = @CustomerId";
        var customer = (await _data.DbConnection.QueryAsync<Customer>(sql, new {CustomerId = id})).FirstOrDefault();

        return customer;
    }

    public async Task<Customer> SaveAsync(Customer customer)
    {
        var id = await _data.DbConnection.InsertAsync<Customer>(customer);
        customer.Id = id;

        return customer;
    }

    public async Task<List<CustomerDto>> SearchAsync(CustomerFilter filter)
    {
        dynamic param = new ExpandoObject();
        var sql = @"SELECT
                    c.Id,
                    c.Name,
                    c.Rfc,
                    c.Phone,
                    c.Email,
                    c.Address,
                    c.StateCode,
                    s.Code,
                    s.CountryCode,
                    s.Name,
                    s.TaxAuthorityCode
                    FROM
                    Customer c INNER JOIN State s ON c.StateCode = s.Code
                    WHERE 1 = 1 ";
        if(!string.IsNullOrEmpty(filter.Name))
        {
            param.CustomerName = $"%{filter.Name}%";
            sql += $" AND c.Name LIKE @CustomerName";
        }

        if(!string.IsNullOrEmpty(filter.Code))
        {
            param.StateCode = $"{filter.Code}";
            sql += $" AND s.Code = @StateCode";
        }

        var customers = await _data.DbConnection.QueryAsync<Customer,State,CustomerDto>(sql,(customer,state) =>
            new CustomerDto
            {
                Id= customer.Id,
                Name = customer.Name,
                Rfc = customer.Rfc,
                Phone = customer.Phone,
                Email = customer.Email,
                Address = customer.Address,
                StateCode = state.Code,
                State = new State
                {
                    Code = state.Code,
                    CountryCode = state.CountryCode,
                    Name = state.Name,
                    TaxAuthorityCode = state.TaxAuthorityCode
                }
            }, param : (object) param,splitOn : "StateCode"
        );

        return customers.ToList();
    }

    public async Task<Customer> UpdateAsync(Customer customer)
    {
        await _data.DbConnection.UpdateAsync<Customer>(customer);

        return customer;
    }
}
