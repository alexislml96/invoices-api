using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeLaSalle.Invoices.Api.Repositories.Interfaces;
using DeLaSalle.Invoices.Entities;
using DeLaSalle.Invoices.Entities.Dto;
using DeLaSalle.Invoices.Entities.Http;

namespace DeLaSalle.Invoices.Api.Repositories;

public class CustomerInMemoryRepository : ICustomerRepository
{

    private readonly List<Customer> _customers;
    public CustomerInMemoryRepository()
    {
        _customers = new List<Customer>();
    }
    public async Task<bool> DeleteAsync(int id)
    {
        return await Task.FromResult(_customers.Remove(_customers.Single(x=>x.Id==id)));
    }

    public async Task<List<Customer>> GetAllAsync()
    {
        // var lst = new List<Customer>();
        // lst.Add(new Customer {Id=1, Name="Alexis"});

        return await Task.FromResult(_customers);
    }

    public async Task<Customer> GetCustomerByIdAsync(int id)
    {
        return await Task.FromResult(_customers.FirstOrDefault(x=> x.Id == id));
    }

    public async Task<Customer> SaveAsync(Customer customer)
    {
        int id = _customers.Count +1;
        customer.Id = id;

        _customers.Add(customer);

        return await Task.FromResult(customer);
    }

    public Task<List<CustomerDto>> SearchAsync(CustomerFilter filter)
    {
        throw new NotImplementedException();
    }

    public async Task<Customer> UpdateAsync(Customer customer)
    {
        var index = _customers.FindIndex(x=> x.Id == customer.Id);
        
        if(index != -1)
        {
            _customers[index] = customer;
        }

        return await Task.FromResult(customer);
    }
}