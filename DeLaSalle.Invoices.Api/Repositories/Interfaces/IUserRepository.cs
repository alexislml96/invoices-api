using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeLaSalle.Invoices.Entities;

namespace DeLaSalle.Invoices.Api.Repositories.Interfaces;
public interface IUserRepository
{
    Task<int> GetAuthorization(AuthEnt auth);
    Task<User> GetUserById(int id);
}
