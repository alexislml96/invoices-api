using DeLaSalle.Invoices.Entities;

namespace DeLaSalle.Invoices.Api.Repositories.Interfaces;

public interface IStateRepository
{
    Task<List<State>> GetByCountryAsync(string countryCode);
}
