using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeLaSalle.Invoices.Entities;
using DeLaSalle.Invoices.Entities.Dto;
using DeLaSalle.Invoices.Entities.Http;

namespace DeLaSalle.Invoices.Api.Repositories.Interfaces;

public interface ICustomerRepository
{
    Task<List<Customer>> GetAllAsync();
    Task<Customer> GetCustomerByIdAsync(int id);
    Task<Customer> SaveAsync(Customer customer);
    Task<Customer> UpdateAsync(Customer customer);
    Task<bool> DeleteAsync(int id);
    Task<List<CustomerDto>> SearchAsync(CustomerFilter filter);
}
