using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DeLaSalle.Invoices.Api.DataAccess.Interfaces;
using DeLaSalle.Invoices.Api.Repositories.Interfaces;
using DeLaSalle.Invoices.Entities;

namespace DeLaSalle.Invoices.Api.Repositories;


public class UserRepository : IUserRepository
{
    private readonly IData _data;
    public UserRepository(IData data)
    {
        _data = data;
    }

    public async Task<int> GetAuthorization(AuthEnt auth)
    {
        var sql = "select UserId from User where UserName = @UserName and Password = @Password";
        var res = await _data.DbConnection.ExecuteScalarAsync<int>(sql, new {UserName = auth.UserName, Password = auth.Password});

        return res;
    }

    public async Task<User> GetUserById(int id)
    {
        var sql = "select * from User where UserId = @UserId";
        var user = (await _data.DbConnection.QueryAsync<User>(sql, new {UserId = id})).FirstOrDefault();

        return user;
    }
}
