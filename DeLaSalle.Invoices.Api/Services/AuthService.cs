using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DeLaSalle.Invoices.Api.Repositories.Interfaces;
using DeLaSalle.Invoices.Api.Services.Interfaces;
using DeLaSalle.Invoices.Entities;
using Microsoft.IdentityModel.Tokens;

namespace DeLaSalle.Invoices.Api.Services;
public class AuthService : IAuthService
{
    private readonly IUserRepository _userRepository;
    public AuthService(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }
    public async Task<UserSession> GetAuthorization(AuthEnt auth)
    {
        var userSession = new UserSession();
        
        var userId = await _userRepository.GetAuthorization(auth);
        if (userId > 0)
        {
            userSession.UserId = userId;
            userSession.Token = GenerateJwtToken(userSession);
        }
        return userSession;
    }

    public string GenerateJwtToken(UserSession userSession)
    {
        var secret = "THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACTE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING";

        var tokenHandler = new JwtSecurityTokenHandler();

        var key = System.Text.Encoding.ASCII.GetBytes(secret);

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[] { new Claim("id", userSession.UserId.ToString()) }),
            Expires = System.DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)

        };

        var token = tokenHandler.CreateToken(tokenDescriptor);

        return tokenHandler.WriteToken(token);
    }

    public async Task<User> GetUserById(int id)
    {
       return await _userRepository.GetUserById(id);
    }


}