
using DeLaSalle.Invoices.Api.Repositories;
using DeLaSalle.Invoices.Api.Repositories.Interfaces;
using DeLaSalle.Invoices.Api.Services.Interfaces;
using DeLaSalle.Invoices.Entities;

namespace DeLaSalle.Invoices.Api.Services;

public class CountryService : ICountryService
{
    private readonly IStateRepository _stateRepository;
    public CountryService(IStateRepository stateRepository)
    {
        _stateRepository = stateRepository;
    }


    public async Task<List<State>> GetStateByCountryCodeAsync(string countryCode)
    {
        return await _stateRepository.GetByCountryAsync(countryCode);
    }
}