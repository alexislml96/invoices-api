using DeLaSalle.Invoices.Entities;
using DeLaSalle.Invoices.Entities.Dto;
using DeLaSalle.Invoices.Entities.Http;

namespace DeLaSalle.Invoices.Api.Services.Interfaces;
public interface ICustomerService
{
    Task<List<Customer>> GetAllAsync();
    Task<Customer> GetCustomerByIdAsync(int id);
    Task<Customer> SaveAsync(Customer customer);
    Task<Customer> UpdateAsync(Customer customer);
    Task<bool> DeleteAsync(int id);

    Task<List<CustomerDto>> SearchAsync(CustomerFilter filter);
}
