using DeLaSalle.Invoices.Entities;
namespace DeLaSalle.Invoices.Api.Services.Interfaces;

public interface ICountryService
{
    Task<List<State>> GetStateByCountryCodeAsync(string countryCode);
}


