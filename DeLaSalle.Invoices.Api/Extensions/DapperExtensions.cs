using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeLaSalle.Invoices.Api.Extensions;
    public static class DapperExtensions
    {
        public static void ConfigureSqlMapperExtensions(string nameSpace)
        {
            Dapper.Contrib.Extensions.SqlMapperExtensions.TableNameMapper = (entityName) => {
                var name = entityName.ToString();
                if(name.Contains(nameSpace))
                {
                    name = name.Replace(nameSpace, string.Empty);
                }

                name = ToUpperCaseLetter(name);

                return name;

            };
        }

        private static string ToUpperCaseLetter(string source)
        {
            if(string.IsNullOrEmpty(source))
                return string.Empty;

            return char.ToUpper(source[0]) + source.Substring(1);

        }
    }
