using System.Data;
using System.Data.Common;

namespace DeLaSalle.Invoices.Api.DataAccess.Interfaces;

    public interface IData
    {
        IDbConnection DbConnection{get;}
    }
