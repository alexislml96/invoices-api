using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeLaSalle.Invoices.Api.DataAccess.Interfaces;
using System.Data;
using System.Data.Common;
using MySqlConnector;

namespace DeLaSalle.Invoices.Api.DataAccess;

    public class Data: IData
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString = "DatabaseConnection";

        public Data(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private MySqlConnection _conn;
        public IDbConnection DbConnection
        {
            get
            {
                if(_conn == null)
                {
                    _conn = new MySqlConnection(_configuration.GetConnectionString(_connectionString));
                }
                return _conn;
            }
        }
    }
