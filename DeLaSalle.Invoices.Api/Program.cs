using DeLaSalle.Invoices.Api.Services.Interfaces;
using DeLaSalle.Invoices.Api.Services;
using DeLaSalle.Invoices.Api.Repositories.Interfaces;
using DeLaSalle.Invoices.Api.Repositories;
using DeLaSalle.Invoices.Api.Extensions;
using DeLaSalle.Invoices.Api.DataAccess.Interfaces;
using DeLaSalle.Invoices.Api.DataAccess;
using DeLaSalle.Invoices.Api.Middleware;

var builder = WebApplication.CreateBuilder(args);

var corsPolicy = "corsPolicy";

builder.Services.AddCors(options =>{
    options.AddPolicy(corsPolicy, configurePolicy => {
        
        configurePolicy.AllowAnyMethod()
            .AllowAnyHeader()
            .WithOrigins("*");
    });
});

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
DapperExtensions.ConfigureSqlMapperExtensions("DeLaSalle.Invoices.Entities.");

builder.Services.AddSingleton<IConfiguration>(builder.Configuration);
builder.Services.AddScoped<IAuthService, AuthService>();
builder.Services.AddScoped<ICustomerService,CustomerService>();
builder.Services.AddScoped<ICountryService,CountryService>();
builder.Services.AddScoped<ICustomerRepository,CustomerRepository>();
builder.Services.AddScoped<IStateRepository,StateRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IData,Data>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

//app.UseExceptionHandler("/api/error");
app.ConfigureExceptionHandler();

app.UseCors(corsPolicy);

app.UseMiddleware<JWTMiddleware>();

app.UseAuthorization();

app.MapControllers();

app.Run();
